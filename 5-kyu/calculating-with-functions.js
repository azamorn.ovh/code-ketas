/*
This time we want to write calculations using functions and get the results. Let's have a look at some examples:

seven(times(five())); // must return 35
four(plus(nine())); // must return 13
eight(minus(three())); // must return 5
six(dividedBy(two())); // must return 3
Requirements:

There must be a function for each number from 0 ("zero") to 9 ("nine")
There must be a function for each of the following mathematical operations: plus, minus, times, dividedBy (divided_by in Ruby and Python)
Each calculation consist of exactly one operation and two numbers
The most outer function represents the left operand, the most inner function represents the right operand
Divison should be integer division. For example, this should return 2, not 2.666666...:
eight(dividedBy(three()));
*/
function zero() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 0, arguments[0].number) }
    else{ return 0; }
}
function one() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 1, arguments[0].number) }
    else{ return 1; }
}
function two() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 2, arguments[0].number) }
    else{ return 2; }
}
function three() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 3, arguments[0].number) }
    else{ return 3; }
}
function four() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 4, arguments[0].number) }
    else{ return 4; }
}
function five() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 5, arguments[0].number) }
    else{ return 5; }
}
function six() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 6, arguments[0].number) }
    else{ return 6; }
}
function seven() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 7, arguments[0].number) }
    else{ return 7; }
}
function eight() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 8, arguments[0].number) }
    else{ return 8; }
}
function nine() {
    if(typeof arguments[0] == 'object'){ return operationResult(arguments[0].operation, 9, arguments[0].number) }
    else{ return 9; }
}

function operationResult(operation, a, b){
    if(operation == 'plus'){ return a + b; }
    else if(operation == 'minus'){ return a - b; }
    else if(operation == 'times'){ return a * b; }
    else if(operation == 'dividedBy'){ return Math.floor(a / b); }
}

function plus() {
    return { operation : 'plus', number : arguments[0] }
}
function minus() {
    return { operation : 'minus', number : arguments[0] }
}
function times() {
    return { operation : 'times', number : arguments[0] }
}
function dividedBy() {
    return { operation : 'dividedBy', number : arguments[0] }
}