/*
To participate in a prize draw each one gives his/her firstname.
Each letter of a firstname has a value which is its rank in the English alphabet. A and a have rank 1, B and b rank 2 and so on.
The length of the firstname is added to the sum of these ranks hence a number som.
An array of random weights is linked to the firstnames and each som is multiplied by its corresponding weight to get what they call a winning number.

Example:
names: "COLIN,AMANDBA,AMANDAB,CAROL,PauL,JOSEPH"
weights: [1, 4, 4, 5, 2, 1]

PauL -> som = length of firstname + 16 + 1 + 21 + 12 = 4 + 50 -> 54
The *weight* associated with PauL is 2 so PauL's *winning number* is 54 * 2 = 108.
Now one can sort the firstnames in decreasing order of the winning numbers. When two people have the same winning number sort them alphabetically by their firstnames.

Task:
parameters: st a string of firstnames, we an array of weights, n a rank
return: the firstname of the participant whose rank is n (ranks are numbered from 1)

Note:
If st is empty return "No participants".
If n is greater than the number of participants then return "Not enough participants".
See Examples Test Cases for more examples.
*/

function rank(st, we, n) {
    if(typeof st != 'string') { return "No participants"; }
    else if(st.length == 0) { return "No participants"; }
    var names = st.split(','), players = [],
    alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
    calculateSom = (name) => {
        let som = name.length;
        for(let i = 0, n = name.length; i < n; i++){
            som += alphabet.indexOf(name.substr(i,1).toLowerCase()) + 1
        }
        return som;
    }
    if(n > names.length) { return "Not enough participants"; }
    for(let i = 0, n = names.length; i < n; i++){
        let som = calculateSom(names[i])
        players[i] = {
            name: names[i],
            winning_number: som * we[i]
        };
    }
    players.sort((a,b) => {
        if(a.winning_number > b.winning_number){ return -1; }
        else if(a.winning_number < b.winning_number){ return 1; }
        else{
            let ntmp = [a.name, b.name]; ntmp.sort();
            if(a.name == ntmp[0]){ return -1; }
            else{ return 1; }
        }
    });
    return players[n-1].name;
}