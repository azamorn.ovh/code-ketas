/*
https://www.codewars.com/kata/5667e8f4e3f572a8f2000039
This time no story, no theory. The examples below show you how to write function accum:

Examples:

accum("abcd") -> "A-Bb-Ccc-Dddd"
accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
accum("cwAt") -> "C-Ww-Aaa-Tttt"
The parameter of accum is a string which includes only letters from a..z and A..Z.
*/

function accum(s) {
    var out = "";
    for(let i = 0, t = 1; i < s.length; i++, t++){
        for(let i2 = 0; i2 < t; i2++){
            i2 == 0 ? out += s[i].toUpperCase() : out += s[i].toLowerCase();
        }
        if(i+1 < s.length)
        out += "-";
    }
    return out;
}