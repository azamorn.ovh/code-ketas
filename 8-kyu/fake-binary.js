/*
https://www.codewars.com/kata/57eae65a4321032ce000002d
Given a string of digits, you should replace any digit below 5 with '0' and any digit 5 and above with '1'. Return the resulting string.
*/
function fakeBin(x) {
    let bin = "";
    for(let i = 0, n = x.length; i < n; i++) x[i] < 5 ? bin += 0 : bin += 1;
    return bin;
}