/*
https://www.codewars.com/kata/57a386117cb1f31890000039
Write function parseFloat (for Javascript parseF) which takes a string and returns a number or Nothing (for Python None, for Javascript null) if conversion is not possible.
*/
function parseF(s) {
    let float = parseFloat(s);
    return !Number.isNaN(float) ? float : null;
  }