/*
https://www.codewars.com/kata/57a0e5c372292dd76d000d7e
Write a function called repeatString which repeats the given string src exactly count times.

repeatStr(6, "I") // "IIIIII"
repeatStr(5, "Hello") // "HelloHelloHelloHelloHello"
*/
function repeatStr(n, s) {
    let o = "";
    for(let i = 0; i < n; i++){ o += s; }
    return o;
}